JavaScript Sort Algorithms
=============


1. Bubble Sort: bubbleSort<br/>
2. Insertion Sort: insertionSort<br/>
3. Selection Sort: selectionSort<br/>
4. Counting Sort: countingSort<br/>
5. Merge Sort: mergeSort<br/>
6. Quick Sort: quickSort<br/>
7. Radix Sort: radixSort<br/>
8. Heap Sort: heapSort<br/>


<b>Usage:</b><br/>
```
import {<SORT API>} from "@btd/JSSortAlgorithms";
<SORT API>(<[Integer Array]>);

Example:

import {bubbleSort} from '@btd/JSSortAlgorithms';
let sortedArray = bubbleSort([5,4,3,2,1]);
````
