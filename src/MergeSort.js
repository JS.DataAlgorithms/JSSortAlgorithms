/**
 * Function to merge two arrays by using merge sort logic
 * @param {Array} firstArray Array of integers
 * @param {Array} secondArray Array of integers
 * @return {Array} Single array of merge sorted values
 */
function _mergeArrays(firstArray, secondArray) {
    let finalArray = [];
    let firstLength;
    let secondLength;
    /**
     * Internally while does a "==" comparison and not "===";
     * hence, if we have the number 0, the condition fails
     * While computing, JS considers 0 as false - try 0 == false -> true
     * Hence, if we have a 0, we change the same to a String and pass the same,
     * else we take the actual value
     **/
    while (firstArray[0] && (secondArray[0] === 0 ? '0' : secondArray[0])) {
        let firstElement = firstArray[0];
        let secondElement = secondArray[0];
        if (firstElement < secondElement) {
            finalArray.push(firstElement);
            firstArray.shift();
        } else if (firstElement > secondElement) {
            finalArray.push(secondElement);
            secondArray.shift();
        } else if (firstElement === secondElement) {
            finalArray.push(firstElement);
            finalArray.push(secondElement);
            firstArray.shift();
            secondArray.shift();
        }
    }
    firstLength = firstArray.length;
    secondLength = secondArray.length;
    if (firstLength !== 0) {
        finalArray = finalArray.concat(firstArray);
    }
    if (secondLength !== 0) {
        finalArray = finalArray.concat(secondArray);
    }
    return finalArray;
}


/**
 * Merge Sort function
 * @param {Array} inputArray Array of integers
 * @return {Array} Sorted array of integers
 */
export default function mergeSort(inputArray) {
    let processArray = [];
    let intermediateArray = [];

    // Break down the elements into their own array
    for (let count = 0; count < inputArray.length; count++) {
        processArray.push([inputArray[count]]);
    }

    let processArrayLength = processArray.length;
    while (processArray[0]) {
        let indexElement = processArray.shift();
        let nextElement = processArray[0] ? processArray.shift() : [];
        intermediateArray.push(_mergeArrays(indexElement, nextElement));

        // Below condition: We are done with the pass; reset variables for
        // next pass
        if (processArrayLength > 1 && !processArray[0]) {
            processArray = intermediateArray;
            intermediateArray = [];
            processArrayLength = processArray.length;
        } else if (processArrayLength === 1) {
            // Above condition: We are done with all passes and have no
            // arrays left. Break and return
            processArray = intermediateArray[0];
            break;
        }
    }
    return processArray;
}
