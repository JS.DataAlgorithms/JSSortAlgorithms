/**
* Insertion Sort function
 * @param {Array} inputArray Array of integers
 * @param {Function} comparator Sort comparator
 * @return {Array} Sorted array of integers
 */
export default function insertionSort( inputArray, comparator = (a, b) => {
    return a > b;
}) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));

    // Perform the look through n-1 times
    for (let count = 1; count < clonedArray.length; count++) {
        if ((clonedArray[count] === 0 ? '0' : clonedArray[count])) {
            // For this number, compare it with n previous numbers
            for (let innerCount = count - 1; innerCount >= 0; innerCount--) {
                let indexNumber = clonedArray[innerCount + 1];
                let prevNumber = clonedArray[innerCount];
                if ( prevNumber > indexNumber ) {
                    // SWAP
                    clonedArray[innerCount] = indexNumber;
                    clonedArray[innerCount + 1] = prevNumber;
                } else {
                    break;
                }
            }
        }
    }
    return clonedArray;
}
