/**
 * Counting Sort function
 * @param {Array} inputArray Array of integers
 * @param {Integer} maxNumber Maximum Number
 * @return {Array} Sorted array of integers
 */
export default function countingSort( inputArray, maxNumber = undefined) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    let inputLength = clonedArray.length;

    if (typeof maxNumber === 'undefined') {
        maxNumber = 0;
        for(let count = 0; count < inputLength; count++) {
            maxNumber = clonedArray[count] > maxNumber ?
                clonedArray[count] : maxNumber;
        }
    }
    let hashArray = new Array(maxNumber+1).fill(0);

    for (let count = 0; count < inputLength; count++) {
        let indexElement = clonedArray[count];
        hashArray[indexElement]++;
    }
    clonedArray = [];

    for (let count = 0; count <= maxNumber; count++) {
        let indexElement = hashArray[count];
        for (let innerCount = 0; innerCount < indexElement; innerCount++) {
            clonedArray.push(count);
        }
    }
    return clonedArray;
}
