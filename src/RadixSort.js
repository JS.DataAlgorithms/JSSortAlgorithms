/**
 * Radix Sort function
 * @param {Array} inputArray Array of integers
 * @return {Array} Sorted array of integers
 */
export default function radixSort( inputArray ) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    const clonedArrayLength = clonedArray.length;
    let inputDigitLength = 0;
    let intermediateObjectArray={};

    for(let count=0; count< clonedArrayLength; count++) {
        let interElement = clonedArray[count].toString();
        let interElementLength = interElement.length;
        if ( interElementLength > inputDigitLength ) {
            inputDigitLength = interElementLength;
        }
    }

    for (let count = 1; count <= inputDigitLength; count++) {
        let iterationExponent = Math.pow(10, count);
        for (let innerCount = 0; innerCount < clonedArrayLength; innerCount++) {
            let innerNumber = clonedArray[innerCount];
            let innerDigit = Math.floor(innerNumber % iterationExponent / ( iterationExponent/10));
            intermediateObjectArray[innerDigit] = intermediateObjectArray[innerDigit] ? intermediateObjectArray[innerDigit] : [];
            intermediateObjectArray[innerDigit].push( innerNumber );
        }
        clonedArray = [];
        for (let innerKey in intermediateObjectArray) {
            clonedArray = clonedArray.concat(intermediateObjectArray[innerKey]);
        }
        intermediateObjectArray={};
    }
    return clonedArray;
}
