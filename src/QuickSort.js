/**
 * Quick Sort function - Only unique values
 * @param {Array} inputArray Array of numbers
 * @param {Integer} startPoint Starting point
 * @param {Integer} endPoint Ending Point
 * @param {Function} comparator Sort comparator
 * @return {Array} Sorted array of integers
 */
export default function quickSort(inputArray, startPoint = 0,
    endPoint = inputArray.length, comparator = (a, b) => {
        return a <= b;
    }) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    let pivot = Math.floor(Math.random() * (endPoint - startPoint)
        + startPoint);
    let pivotElement = clonedArray[pivot];
    let endCountTrack = endPoint;
    for (let startCount = 0; startCount < endPoint; startCount++) {
        let startElement = clonedArray[startCount];
        if (startElement === clonedArray[endCountTrack]) {
            break;
        }
        if (startElement >= pivotElement) {
            for (let endCount = endCountTrack; endCount >= startCount;
                endCount--) {
                let endElement = clonedArray[endCount];
                if (comparator(endElement, pivotElement)) {
                    endCountTrack = endCount;
                    // Swap
                    clonedArray[startCount] = endElement;
                    clonedArray[endCount] = startElement;
                    startCount--;
                    break;
                }
            }
        }
    }
    if (startPoint < pivot && (pivot - startPoint) > 1) {
        quickSort(clonedArray, startPoint, pivot + 1);
    }
    if (pivot < endPoint && (endPoint - pivot) > 1) {
        quickSort(clonedArray, pivot, endPoint);
    }
    return clonedArray;
}
