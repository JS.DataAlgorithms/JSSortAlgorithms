/**
 * Selection Sort function
 * @param {Array} inputArray Array of integers
 * @return {Array} Sorted array of integers
 */
export default function selectionSort( inputArray ) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    let inputLength = clonedArray.length;
    // We only need to do n-1 runs because we keep swapping
    // n-1 record will be compared with the nth record and will be swapped
    // accordingly.
    // By the time n-1 is reached, n and n-1 will be the largest two records
    // left

    for (let count = 0; count < inputLength - 1; count++) {
        let indexElement = clonedArray[count];
        let smallestElement = indexElement;
        let smallestIndex = count;

        for (let innerCount = count + 1; innerCount < inputLength; innerCount++) {
            let compElement = clonedArray[innerCount];
            if ( compElement < smallestElement ) {
                smallestIndex = innerCount;
                smallestElement = compElement;
            }
        }
        if ( smallestIndex !== count ) {
            // SWAP
            clonedArray[count] = smallestElement;
            clonedArray[smallestIndex] = indexElement;
        }
    }
    return clonedArray;
}
