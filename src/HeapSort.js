/**
 * Min Heapify an array. This is a recursive function which will result
 * in the smallest number as being the root
 * @param {Array} inputArray Array of integers
 * @param {Integer} count parent index
 * @return {Array} modified array
 */
function _minHeapify(inputArray, count) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    if (clonedArray.length > 1) {
        let element = clonedArray[count];
        let parentIndex = Math.ceil((count - 2) / 2);
        let parentElement = clonedArray[parentIndex];
        if (count !== parentIndex) {
            if (element < parentElement) {
                clonedArray[count] = parentElement;
                clonedArray[parentIndex] = element;
                _minHeapify(clonedArray, parentIndex);
            }
        }
    }
    return clonedArray;
}

/**
 * Heap Sort function
 * @param {Array} inputArray Array of integers
 * @return {Array} Sorted array of integers
 */
export default function heapSort(inputArray) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    let inputLength = clonedArray.length;
    let returnArray = [];

    while (inputLength > 0) {
        for (let count = 0; count < inputLength; count++) {
            clonedArray = _minHeapify(clonedArray, count);
        }
        returnArray.push(clonedArray.shift());
        inputLength = clonedArray.length;
    }
    return returnArray;
}
