/**
 * Bubble Sort function
 * @param {Array} inputArray Array of integers
 * @param {Function} comparator Sort comparator
 * @return {Array} Sorted array of integers
 */
export default function bubbleSort(inputArray, comparator = (a, b) => {
    return a > b;
}) {
    let clonedArray = JSON.parse(JSON.stringify(inputArray));
    const inputLength = clonedArray.length;
    // Perform the sort n-1 times
    for (let count = 0; count < inputLength - 1; count++) {
        // Go through the array while stopping at n-count each time
        for (let innerCount = 0; innerCount < inputLength - count;
            innerCount++) {
            let indexNumber = clonedArray[innerCount];
            let nextNumber = clonedArray[innerCount + 1];
            if (comparator(indexNumber, nextNumber)) {
                // Swap
                clonedArray[innerCount] = nextNumber;
                clonedArray[innerCount + 1] = indexNumber;
            }
        }
    }
    return clonedArray;
};