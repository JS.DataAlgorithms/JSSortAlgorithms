import {expect} from 'chai';
import bubbleSort from '../src/BubbleSort';

describe('Bubble Sort', function() {
    it('should return an array', function() {
        expect(bubbleSort([1, 2])).to.be.instanceof(Array);
    });
    it('should work with an empty array', function() {
        expect(bubbleSort([])).to.eql([]);
    });
    for (let count = 1; count < 4; count++) {
        it('should return a sorted array of 10000 numbers. Count: ' + count,
            function() {
                const randomArray = Array.from({length: 10000},
                    () => Math.floor(Math.random() * 1000));
                const sortedArray = randomArray.sort((a, b) => {
                    return a - b;
                });
                expect(bubbleSort(randomArray)).to.eql(sortedArray);
            });
    }
});
