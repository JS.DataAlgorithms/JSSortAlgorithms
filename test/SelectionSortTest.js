import {expect} from 'chai';
import selectionSort from '../src/SelectionSort';

describe('Selection Sort', function() {
    it('should return an array', function() {
        expect(selectionSort([1, 2])).to.be.instanceof(Array);
    });
    it('should work with an empty array', function() {
        expect(selectionSort([])).to.eql([]);
    });
    for (let count = 1; count < 4; count++) {
        it('should return a sorted array of 10000 numbers. Count: ' + count,
            function() {
                const randomArray = Array.from({length: 10000},
                    () => Math.floor(Math.random() * 1000));
                const sortedArray = randomArray.sort((a, b) => {
                    return a - b;
                });
                expect(selectionSort(randomArray)).to.eql(sortedArray);
            });
    }
});
