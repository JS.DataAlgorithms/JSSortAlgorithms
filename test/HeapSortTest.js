import {expect} from 'chai';
import heapSort from '../src/HeapSort';

describe('Heap Sort', function() {
    it('should return an array', function() {
        expect(heapSort([1, 2])).to.be.instanceof(Array);
    });
    it('should work with an empty array', function() {
        expect(heapSort([])).to.eql([]);
    });
    for (let count = 1; count < 4; count++) {
        it('should return a sorted array of 100 numbers. Count: ' + count,
            function() {
                const randomArray = Array.from({length: 100},
                    () => Math.floor(Math.random() * 1000));
                const sortedArray = randomArray.sort((a, b) => {
                    return a - b;
                });
                expect(heapSort(randomArray)).to.eql(sortedArray);
            });
    }
});
