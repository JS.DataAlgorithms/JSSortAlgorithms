import {expect} from 'chai';
import quickSort from '../src/QuickSort';

describe('Quick Sort', function() {
    it('should return an array', function() {
        expect(quickSort([1, 2])).to.be.instanceof(Array);
    });
    it('should work with an empty array', function() {
        expect(quickSort([])).to.eql([]);
    });
    for (let count = 1; count < 4; count++) {
        it('should return a sorted array of 1000 numbers. Count: ' + count,
            function() {
                const randomArray = Array.from({length: 1000},
                    () => Math.floor(Math.random() * 1000));
                const sortedArray = randomArray.sort((a, b) => {
                    return a - b;
                });
                expect(quickSort(randomArray)).to.eql(sortedArray);
            });
    }
});
